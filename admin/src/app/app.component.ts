import {Component, ViewEncapsulation} from '@angular/core';
import {BehaviorSubject, timer} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {
    user$ = new BehaviorSubject({mainRole: 'Главный специалист', fio: 'Миронова Анна Владимировна'});
    days = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
    months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
    now$ = timer(0, 120000).pipe(map(_ => new Date()));

    constructor() {
    }

    getMinutes(date: Date): string {
        return date ? this.getTime(date.getMinutes()) : '';
    }

    getHours(date: Date): string {
        return date ? this.getTime(date.getHours()) : '';
    }

    private getTime(t: number): string {
        return (t < 10 ? '0' : '') + t;
    }
}
