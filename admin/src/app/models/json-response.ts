export interface JsonResponse<T> {
    status: boolean;
    data: T;
}
