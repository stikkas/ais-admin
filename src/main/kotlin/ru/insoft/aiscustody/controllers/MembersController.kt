package ru.insoft.aiscustody.controllers

import org.springframework.web.bind.annotation.*
import ru.insoft.aiscustody.models.Classifier
import ru.insoft.aiscustody.services.MembersService

@RestController
@RequestMapping("/api/members")
class MembersController(private val membersService: MembersService) : AbstractController() {
    @GetMapping("/relatives")
    fun getRelatives(@RequestParam ids: List<String>) = handle {
        membersService.findRelatives(ids)
    }

    @GetMapping("/children/{systemmanId}")
    fun getChildren(@PathVariable systemmanId: String) = handle {
        membersService.getChildren(systemmanId)
    }

    @GetMapping("/{mainId}")
    fun searchMembersOfChild(@PathVariable mainId: String, @RequestParam(defaultValue = "") childs: List<String>) = handle {
        membersService.findAll(mainId, childs)
    }

    @PostMapping("/{mainId}")
    fun addMember(@PathVariable mainId: String, @RequestParam systemmanId: String, @RequestParam relation: Classifier) = handle {
        membersService.addMember(mainId, systemmanId, relation)
    }
}

