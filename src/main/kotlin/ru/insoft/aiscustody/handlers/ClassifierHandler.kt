package ru.insoft.aiscustody.handlers

import org.apache.ibatis.type.EnumTypeHandler
import org.apache.ibatis.type.JdbcType
import ru.insoft.aiscustody.models.Classifier
import java.sql.PreparedStatement

class ClassifierHandler : EnumTypeHandler<Classifier>(Classifier::class.java) {
    override fun setNonNullParameter(ps: PreparedStatement, i: Int, parameter: Classifier, jdbcType: JdbcType?) {
        ps.setString(i, parameter.id);
    }
}
