package ru.insoft.aiscustody.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.aiscustody.filters.LdFilter
import ru.insoft.aiscustody.models.LdTableRow

@Mapper
interface LdMapper {
    fun findAll(@Param("filter") filter: LdFilter, @Param("offset") offset: Long, @Param("size") size: Int): List<LdTableRow>
    fun count(@Param("filter") filter: LdFilter): Long
}
