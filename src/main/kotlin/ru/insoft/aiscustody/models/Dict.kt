package ru.insoft.aiscustody.models

import org.apache.ibatis.annotations.Param

data class Dict(@Param("value") val value: String, @Param("label") val label: String)
