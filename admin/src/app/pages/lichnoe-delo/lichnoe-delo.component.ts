import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Person} from '../../models/person';
import {ActivatedRoute} from '@angular/router';

@Component({
    templateUrl: './lichnoe-delo.component.html',
    encapsulation: ViewEncapsulation.None
})
export class LichnoeDeloComponent implements OnInit {
    data: Person;

    constructor(route: ActivatedRoute) {
        route.queryParamMap.subscribe(res => {
            console.log(res);
        });
    }

    ngOnInit() {
    }

}
