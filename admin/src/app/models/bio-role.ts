export enum BioRole {
    Mother = 'MOTHER',
    Father = 'FATHER',
    Sister = 'SISTER',
    Brother = 'BROTHER',
    Ancle = 'ANCLE',
    Aunt = 'AUNT',
    Grandma = 'GRANDMA',
    Grandpa = 'GRANDPA'
}

