package ru.insoft.aiscustody.controllers

import mu.KotlinLogging
import ru.insoft.aiscustody.models.Response

private val logger = KotlinLogging.logger {}

abstract class AbstractController {
    protected fun handle(block: () -> Any?) = try {
        Response.ok(block())
    } catch (ex: Exception) {
        logger.error(ex) {}
        Response.fail(ex.message)
    }
}
