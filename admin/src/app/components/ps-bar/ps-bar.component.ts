import {
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ViewChild,
    ViewEncapsulation
} from '@angular/core';
import {DictService} from '../../services/dict.service';
import {Dict} from '../../models/dict';
import {DictName} from '../../models/dict-name';
import {Observable} from 'rxjs';

@Component({
    selector: 'app-ps-bar',
    templateUrl: './ps-bar.component.html',
    encapsulation: ViewEncapsulation.None
})
export class PsBarComponent implements OnInit {

    @Input() model = {} as any;
    @Input() hasRelation: boolean;
    @Output() search = new EventEmitter();

    @ViewChild('rl', {static: false}) rl: ElementRef;
    members$: Observable<Dict[]>;

    constructor(ds: DictService) {
        this.members$ = ds.getDict(DictName.FamilyMember);
    }

    ngOnInit() {
    }

    relation() {
        const ne = this.rl.nativeElement;
        return ne.options[ne.selectedIndex].label;
    }

    relationId(label: string) {
        const option: any = Array.from(this.rl.nativeElement.options).find((it: any) => it.label === label);
        return option ? option.value : '';
    }
}

