import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NotifierService} from 'angular-notifier';

@Component({
    selector: 'app-bf-header',
    templateUrl: './bf-header.component.html',
    encapsulation: ViewEncapsulation.None
})
export class BfHeaderComponent implements OnInit {
    buttons = [{
        title: 'Сохранить',
        clazz: 'save',
        click: () => {
            this.ns.notify('info', 'Данные сохранены');
        }
    }, {
        title: 'Печать',
        mclazz: 'print',
        clazz: 'print',
        click: () => {
        }
    }, {
        title: 'Выход',
        mclazz: 'exit',
        clazz: 'exit',
        click: () => {
        }
    }];

    constructor(private ns: NotifierService) {
    }

    ngOnInit() {
    }

}
