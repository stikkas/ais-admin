package ru.insoft.aiscustody.services

import ru.insoft.aiscustody.models.Classifier
import ru.insoft.aiscustody.models.LdTableRow
import ru.insoft.aiscustody.models.Members

interface MembersService {
    fun findAll(mainId: String, childs: List<String>): Members
    fun addMember(mainId: String, systemmanId: String, relation: Classifier)
    fun getChildren(systemmanId: String): List<LdTableRow>
    fun findRelatives(mainIds: List<String>): List<LdTableRow>
}
