import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DictName} from '../models/dict-name';
import {BehaviorSubject, Subject} from 'rxjs';
import {Dict} from '../models/dict';

@Injectable({
    providedIn: 'root'
})
export class DictService {
    private baseUrl = '/api/private/dicts/';
    private dicts = {} as { [key: string]: Subject<Dict[]> };

    constructor(private http: HttpClient) {
        this.load(DictName.FamilyMember);
    }

    load(dict: DictName) {
        if (!this.dicts[dict]) {
            const subject = new BehaviorSubject(null);
            this.dicts[dict] = subject;
            this.http.get(`${this.baseUrl}${dict}`).subscribe(res => {
                if (res) {
                    subject.next(res);
                }
            });
        }
    }

    getDict(dict: DictName) {
        return this.dicts[dict].asObservable();
    }
}


