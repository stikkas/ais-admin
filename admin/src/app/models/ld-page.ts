import {Person} from './person';

export interface LdPage {
    count: number;
    item: Person[];
}
