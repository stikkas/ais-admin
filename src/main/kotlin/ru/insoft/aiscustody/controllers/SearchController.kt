package ru.insoft.aiscustody.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.insoft.aiscustody.filters.LdFilter
import ru.insoft.aiscustody.services.SearchService

@RestController
@RequestMapping("/api/search")
class SearchController(private val searchService: SearchService) : AbstractController() {
//    @Value("\${search.url.ld}")
//    private lateinit var searchUrlLd: String;

//    @GetMapping("/ld")
//    fun searchLd(filter: LdFilter, @RequestParam pageSize: Int,
//                 @RequestParam(required = false) sortField: String?,
//                 @RequestParam(required = false) asc: Boolean?): Response {
//        var params = filter.toMap() + ("pageSize" to pageSize)
//        if (sortField != null) {
//            params += "sortField" to sortField
//            params += "asc" to (asc ?: false)
//        }
//        val (request, response, result) = searchUrlLd
//                .httpPost()
//                .jsonBody(om.writeValueAsString(params))
//                .responseObject<LdPage>()
//        return when (result) {
//            is Result.Failure -> {
//                val ex = result.getException()
//                Response.fail(ex.message)
//            }
//            is Result.Success -> {
//                Response.ok(result.get())
//            }
//        }
//    }

    @GetMapping("/ld")
    fun searchPopulation(filter: LdFilter, @RequestParam(defaultValue = "20") pageSize: Int,
                         @RequestParam(defaultValue = "1") page: Long) = handle {
        searchService.findAllLd(filter, page, pageSize)
    }
}


