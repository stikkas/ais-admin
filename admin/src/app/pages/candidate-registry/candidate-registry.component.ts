import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-candidate-registry',
  templateUrl: './candidate-registry.component.html',
  styles: [],
  encapsulation: ViewEncapsulation.None
})
export class CandidateRegistryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
