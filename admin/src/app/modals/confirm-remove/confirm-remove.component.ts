import {ChangeDetectionStrategy, Component, EventEmitter, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {ModalComponent} from '../modal/modal.component';

@Component({
    selector: 'app-confirm-remove',
    templateUrl: './confirm-remove.component.html',
    styleUrls: ['./confirm-remove.component.css'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmRemoveComponent {
    @ViewChild('mc', {static: true}) mc: ModalComponent;
    @Output() ok = new EventEmitter();

    da() {
        this.ok.emit();
        this.mc.close();
    }
}

