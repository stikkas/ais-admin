import {BioRole} from './bio-role';

export interface Person {
    fio: string;
    birthDate: Date | string;
    deathDate?: Date | string;
    documentType: string;
    seria: string;
    number: string;
    sex?: string;
    sibling?: boolean;
    role?: BioRole;
    remark?: string;
    lastName?: string;
    firstName?: string;
    middleName?: string;

    systemmanId?: string;
    systemAddressId?: string;
    address?: string;
    registrationDate?: string;
    registrationId?: string;
    flat?: string;
}
