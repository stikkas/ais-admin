import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styles: [],
  encapsulation: ViewEncapsulation.None
})
export class ReportsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
