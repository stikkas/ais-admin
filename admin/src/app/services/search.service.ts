import {Injectable} from '@angular/core';
import {NotifierService} from 'angular-notifier';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {JsonResponse} from '../models/json-response';
import {LoadingService} from './loading.service';
import {FormBuilder} from '@angular/forms';
import {SortEvent} from '../models/sort-event';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {LdTableRow} from '../models/ld-table-row';
import {Page} from '../models/page';
import {Responsable} from './responsable';
import {LdFilter} from '../models/ld-filter';

@Injectable({
    providedIn: 'root'
})
export class SearchService extends Responsable {
    private baseUrl = '/api/search/';

    private pages$ = {} as { [key: string]: Subject<Page<LdTableRow>> };

    constructor(private http: HttpClient, ns: NotifierService,
                private ls: LoadingService, private fb: FormBuilder,
                private parserFormatter: NgbDateParserFormatter) {
        super(ns);
    }

    // get formGroup() {
    //     return this.fb.group({
    //         relativeId: '',
    //         firstName: '',
    //         lastName: '',
    //         middleName: '',
    //         birthDate: ['', Validators.required],
    //         docNumber: '',
    //         child: false
    //     });
    // }

    findPeople(params: LdFilter, key: string, sort?: SortEvent) {
        this.ls.load(true);
        this.handle(
            this.http.get<JsonResponse<Page<LdTableRow>>>(`${this.baseUrl}ld`, {params: this.getParams(params, sort)})
        ).subscribe(res => {
            this.ls.load(false);
            this.pages$[key].next(res);
        });
    }

    getPeople(key: string): Observable<Page<LdTableRow>> {
        if (!this.pages$[key]) {
            this.pages$[key] = new Subject<Page<LdTableRow>>();
        }
        return this.pages$[key].asObservable();
    }

    /**
     * Очищает резултаты поиска
     * @param searchKey - идентификатор пользователя сервисом
     */
    clear(searchKey: string) {
        const obs = this.pages$[searchKey];
        if (obs) {
            obs.next(null);
        }
    }

    private getParams(p: LdFilter, sort?: SortEvent): any {
        const params = {pageSize: 10} as any;
        Object.keys(p).forEach(k => {
            const v = p[k];
            if (v) {
                params[k] = typeof v === 'object' ? this.parserFormatter.format(v) : v;
            }
        });
        if (sort && sort.direction) {
            params.sortField = sort.column;
            params.desc = sort.direction === 'desc';
        }
        return params;
    }

}


