import {Person} from './person';

export interface BioFamily {
    children: Person[];
    relatives: Person[];
}
