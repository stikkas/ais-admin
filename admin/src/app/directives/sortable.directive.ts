import {Directive, ElementRef, EventEmitter, HostListener, Input, Output, Renderer2} from '@angular/core';
import {SortDirection} from '../models/sort-direction';
import {SortEvent} from '../models/sort-event';

const rotate: { [key: string]: SortDirection } = {asc: 'desc', desc: '', '': 'asc'};
const upClass = 'b-table-sort_mod_up';
const downClass = 'b-table-sort_mod_down';
const childSel = '.b-table-sort';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[sortable]',
})
export class SortableDirective {
    @Input() sortable: string;
    @Output() sort = new EventEmitter<SortEvent>();
    private direction: SortDirection = '';

    constructor(private elem: ElementRef, private renderer: Renderer2) {
    }

    clear(direction?: SortDirection) {
        const child = this.elem.nativeElement.querySelector(childSel);
        this.renderer.removeClass(child, upClass);
        this.renderer.removeClass(child, downClass);
        this.direction = direction || '';
    }

    @HostListener('click')
    rotate() {
        const direction = rotate[this.direction];
        this.clear(direction);
        if (direction) {
            this.renderer.addClass(this.elem.nativeElement.querySelector(childSel),
                direction === 'asc' ? upClass : downClass);
        }
        this.sort.emit({column: this.sortable, direction});
    }
}

