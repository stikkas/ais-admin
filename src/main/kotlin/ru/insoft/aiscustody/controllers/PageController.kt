package ru.insoft.aiscustody.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class PageController {
    @GetMapping(value = [
        "/children-registered",
        "/childrens-registry",
        "/candidate-registry",
        "/family-registry",
        "/biological-family",
        "/reporting-forms",
        "/premises-registry"
    ])
    fun pages() = "/"
}


