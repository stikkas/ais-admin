import {Path} from './path';

export const actions = [{
    title: 'Реестр детей, поставленных на учет',
    link: `/${Path.Registered}`,
    clazz: Path.Registered
}, {
    title: 'Реестр детей (ГБДД)',
    link: `/${Path.KidRegistry}`,
    clazz: Path.KidRegistry
}, {
    title: 'Реестр кандидатов',
    link: `/${Path.CandidateRegistry}`,
    clazz: Path.CandidateRegistry
}, {
    title: 'Реестр семей опеки',
    link: `/${Path.FamilyRegistry}`,
    clazz: Path.FamilyRegistry
}, {
    title: 'Биологическая семья',
    link: `/${Path.BioFamily}`,
    clazz: Path.BioFamily
}, {
    title: 'Отчетные формы',
    link: `/${Path.Reports}`,
    clazz: Path.Reports
}, {
    title: 'Реестр жилых помещений',
    link: `/${Path.PremisesRegistry}`,
    clazz: Path.PremisesRegistry
}];

