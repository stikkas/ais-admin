package ru.insoft.aiscustody.models

data class Members(var kids: List<LdTableRow>, var parents: List<LdTableRow>,
                   var relatives: List<LdTableRow>, var siblings: List<String>)
