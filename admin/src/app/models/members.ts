import {LdTableRow} from './ld-table-row';

export interface Members {
    kids: LdTableRow[];
    siblings: string[];
    parents: LdTableRow[];
    relatives: LdTableRow[];
}
