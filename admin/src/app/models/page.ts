export interface Page<T> {
    /**
     * Найденные данные
     */
    data: T[];

    /**
     * Максимально допустимый размер на странице
     */
    size: number;

    /**
     * Всего найдено
     */
    count: number;

    /**
     * Номер страницы, начинается с 1
     */
    page: number;

    /**
     * Кол-во страниц
     */
    pages: number;

    /**
     * Первая страница
     */
    first: boolean;

    /**
     * Последняя страница
     */
    last: boolean;

}
