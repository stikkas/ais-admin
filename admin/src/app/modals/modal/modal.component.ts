import {Component, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {NgxSmartModalComponent} from 'ngx-smart-modal';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ModalComponent implements OnInit {
    @Input() id: string;
    @Input() title: string;
    @Input() clazz: string;
    @Input() closable: boolean;

    @ViewChild('myModal', {static: true})
    modal: NgxSmartModalComponent;

    constructor() {
    }

    ngOnInit() {
    }

    close() {
        this.modal.close();
    }
}
