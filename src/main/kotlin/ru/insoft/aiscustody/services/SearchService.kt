package ru.insoft.aiscustody.services

import ru.insoft.aiscustody.filters.LdFilter
import ru.insoft.aiscustody.models.LdTableRow
import ru.insoft.aiscustody.models.Members
import ru.insoft.aiscustody.models.Page

interface SearchService {
    fun findAllLd(filter: LdFilter, page: Long, size: Int): Page<LdTableRow>
    fun findAllMembers(mainId: String, childs: List<String>): Members
}
