package ru.insoft.aiscustody

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import kotlin.math.max

@SpringBootApplication
class AisCustodyApplication {
    @Bean
    fun om() = jacksonObjectMapper()
}

fun main(args: Array<String>) {
    runApplication<AisCustodyApplication>(*args)
}

inline fun Long.offset(size: Int) = (max(this, 1) - 1) * size
