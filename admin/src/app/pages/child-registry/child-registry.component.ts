import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-child-registry',
  templateUrl: './child-registry.component.html',
  styles: [],
  encapsulation: ViewEncapsulation.None
})
export class ChildRegistryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
