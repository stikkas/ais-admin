import {NotifierService} from 'angular-notifier';

export abstract class Errorable {
    protected constructor(protected ns: NotifierService) {
    }

    errorHandler(err) {
        if (err.message) {
            this.ns.notify('error', err.message);
        } else {
            console.log(err);
        }
        return null;
    }
}
