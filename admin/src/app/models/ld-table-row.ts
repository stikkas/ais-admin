export interface LdTableRow {
    systemmanId: string;
    fio: string;
    sex: string;
    birthDate: string;
    deathDate: string;
    documentType: string;
    seria: string;
    number: string;
    remark?: string;
    relativeId?: string;
}

