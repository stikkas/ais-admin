import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-child-registered',
  templateUrl: './child-registered.component.html',
  styles: [],
  encapsulation: ViewEncapsulation.None
})
export class ChildRegisteredComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
