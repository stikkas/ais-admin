export enum Path {
    Registered = 'children-registered',
    KidRegistry = 'childrens-registry',
    CandidateRegistry = 'candidate-registry',
    FamilyRegistry = 'family-registry',
    BioFamily = 'biological-family',
    Reports = 'reporting-forms',
    PremisesRegistry = 'premises-registry'
}

