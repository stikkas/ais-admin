import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    Output,
    ViewChild,
    ViewEncapsulation
} from '@angular/core';
import {SortEvent} from '../../models/sort-event';
import {Person} from '../../models/person';
import {TableComponent} from '../table/table.component';
import {LdTableRow} from '../../models/ld-table-row';
import {Page} from '../../models/page';

@Component({
    selector: 'app-search-result',
    templateUrl: './search-result.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchResultComponent {
    @Input() data: Page<LdTableRow>;
    @Input() current: Person;
    @Output() sort = new EventEmitter<SortEvent>();
    @Output() rowClick = new EventEmitter<LdTableRow>();
    @Output() rowDblclick = new EventEmitter<LdTableRow>();

    @ViewChild('at', {static: true}) at: TableComponent;

    sorts = ['surname', 'birthdate', 'sex', 'document', 'seria', 'number'];
    headers = ['ФИО', 'Дата рождения', 'Пол', 'Документ', 'Серия', 'Номер'];
    maxRows = 5;

    clearSorts() {
        this.at.clearSorts();
    }
}

