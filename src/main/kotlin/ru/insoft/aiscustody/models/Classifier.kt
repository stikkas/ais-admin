package ru.insoft.aiscustody.models

enum class Classifier(val id: String) {
    FAMILY_MEMBERS("ce50b7a2-5d0c-11e9-b560-8f05d7884215"), //Родственные отношения
    BROTHER("ce5105af-5d0c-11e9-b564-13e290045461"),
    SISTER("ce559a06-5d0c-11e9-b592-9b0d463fe969"),
    MOTHER("ce5572ec-5d0c-11e9-b591-c71858fc9f93"),
    FATHER("ce5105ae-5d0c-11e9-b563-1f3558911481"),
    ANLCE("ce51a1d0-5d0c-11e9-b56a-4beff454cdfb"),
    AUNT("ce560f23-5d0c-11e9-b598-1709879147fe"),
    GRANDMA("ce559a07-5d0c-11e9-b593-63f05168b18e"),
    GRANDPA("ce512cb4-5d0c-11e9-b565-6b2bee5a7263");

    companion object {
        fun toClassifier(s: String) = values().find { it.id == s }
    }

    override fun toString() = id
}

