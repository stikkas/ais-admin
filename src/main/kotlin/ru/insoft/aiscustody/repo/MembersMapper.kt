package ru.insoft.aiscustody.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.aiscustody.models.Classifier
import ru.insoft.aiscustody.models.LdTableRow

@Mapper
interface MembersMapper {
    fun findParents(@Param("mainId") mainId: String,
                    @Param("cls") cls: List<Classifier> = listOf(Classifier.MOTHER, Classifier.FATHER)): List<LdTableRow>

    fun findRelatives(@Param("mainId") mainId: String,
                      @Param("cls") parents: List<Classifier> = listOf(Classifier.MOTHER, Classifier.FATHER)): List<LdTableRow>

    fun findSiblings(@Param("childs") childs: List<String>, @Param("excludes") excludes: List<String>,
                     @Param("cls") cls: List<Classifier> = listOf(Classifier.BROTHER, Classifier.SISTER)): List<LdTableRow>

    fun addRelation(@Param("mainId") mainId: String, @Param("systemmanId") systemmanId: String,
                    @Param("relation") relation: Classifier)

    fun getChildren(@Param("systemmanId") systemmanId: String,
                    @Param("cls") relations: List<Classifier> = listOf(Classifier.MOTHER, Classifier.FATHER)): List<LdTableRow>

    fun getOtherChildren(@Param("ids") ids: List<String>, @Param("excludes") all: List<String>,
                         @Param("cls") relations: List<Classifier> = listOf(Classifier.MOTHER, Classifier.FATHER)): List<LdTableRow>

    fun findRelativesOfChilds(@Param("mainIds") mainIds: List<String>,
                              @Param("cls") cls: List<Classifier> = listOf(Classifier.BROTHER, Classifier.SISTER)): List<LdTableRow>
}

