import {ChangeDetectionStrategy, Component, EventEmitter, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {ModalComponent} from '../modal/modal.component';

@Component({
    selector: 'app-confirm-save',
    templateUrl: './confirm-save.component.html',
    styleUrls: ['./confirm-save.component.css'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmSaveComponent {

    @ViewChild('mc', {static: true}) mc: ModalComponent;
    @Output() ok = new EventEmitter();

    da() {
        this.ok.emit();
        this.mc.close();
    }
}

