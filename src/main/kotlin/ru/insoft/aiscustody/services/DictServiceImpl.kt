package ru.insoft.aiscustody.services

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.aiscustody.models.Classifier
import ru.insoft.aiscustody.repo.ClassifierMapper

@Service
@Transactional(readOnly = true)
class DictServiceImpl(private val classifierMapper: ClassifierMapper) : DictService {
    override fun classifier(cls: Classifier) = classifierMapper.classifier(cls)
}
