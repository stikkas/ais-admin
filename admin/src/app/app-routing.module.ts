import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from './pages/main/main.component';
import {MainHeaderComponent} from './pages/main/main-header/main-header.component';
import {Path} from './consts/path';
import {BioFamilyComponent} from './pages/bio-family/bio-family.component';
import {BfHeaderComponent} from './pages/bio-family/bf-header/bf-header.component';
import {CandidateRegistryComponent} from './pages/candidate-registry/candidate-registry.component';
import {ChildRegisteredComponent} from './pages/child-registered/child-registered.component';
import {ChildRegistryComponent} from './pages/child-registry/child-registry.component';
import {FamilyRegistryComponent} from './pages/family-registry/family-registry.component';
import {PremsRegistryComponent} from './pages/prems-registry/prems-registry.component';
import {ReportsComponent} from './pages/reports/reports.component';
import {LichnoeDeloComponent} from './pages/lichnoe-delo/lichnoe-delo.component';

const routes: Routes = [{
    path: '',
    pathMatch: 'full',
    children: [
        {path: '', component: MainComponent},
        {path: '', component: MainHeaderComponent, outlet: 'header'}
    ]
}, {
    path: Path.BioFamily,
    children: [
        {path: '', component: BioFamilyComponent},
        {path: '', component: BfHeaderComponent, outlet: 'header'}
    ]
}, {
    path: Path.CandidateRegistry,
    component: CandidateRegistryComponent
}, {
    path: Path.Registered,
    component: ChildRegisteredComponent
}, {
    path: Path.KidRegistry,
    component: ChildRegistryComponent
}, {
    path: Path.FamilyRegistry,
    component: FamilyRegistryComponent
}, {
    path: Path.PremisesRegistry,
    component: PremsRegistryComponent
}, {
    path: Path.Reports,
    component: ReportsComponent
}, {
    path: 'lichnoe-delo',
    component: LichnoeDeloComponent
}];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

