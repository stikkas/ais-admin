package ru.insoft.aiscustody.services

import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.aiscustody.models.Classifier
import ru.insoft.aiscustody.models.LdTableRow
import ru.insoft.aiscustody.models.Members
import ru.insoft.aiscustody.repo.MembersMapper

@Service
@Transactional(readOnly = true)
class MembersServiceImpl(private val membersMapper: MembersMapper) : MembersService {

    override fun findAll(mainId: String, childs: List<String>) = runBlocking {
        val parents = async { membersMapper.findParents(mainId) }
        val relatives = async { membersMapper.findRelatives(mainId) }
        val siblings = async { membersMapper.findSiblings(listOf(mainId), listOf(mainId)) }
        var all = childs + mainId
        var kids = membersMapper.findSiblings(all, all)
        var allkids = kids
        while (kids.isNotEmpty()) {
            val found = kids.map { it.systemmanId!! }
            all = found + all
            kids = membersMapper.findSiblings(found, all)
            allkids += kids
        }
        Members(allkids, parents.await(), relatives.await(), siblings.await().map { it.systemmanId!! })
    }

    @Transactional
    override fun addMember(mainId: String, systemmanId: String, relation: Classifier) {
        membersMapper.addRelation(mainId, systemmanId, relation)
    }

    override fun getChildren(systemmanId: String): List<LdTableRow> {
        var children = membersMapper.getChildren(systemmanId)
        var all = listOf<String>()
        var result = children
        while (children.isNotEmpty()) {
            val ids = children.map { it.systemmanId!! }
            all += ids
            children = membersMapper.getOtherChildren(ids, all)
            result += children
        }
        return result
    }

    override fun findRelatives(mainIds: List<String>): List<LdTableRow> = membersMapper.findRelativesOfChilds(mainIds)
}
