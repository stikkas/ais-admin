import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {LdTableRow} from './ld-table-row';

export class LdFilter {
    birthDate: NgbDateStruct;
    lastName: string;
    firstName: string;
    middleName: string;
    docNumber: string;
    relativeId: string;
    child = false;

    constructor(ld?: LdTableRow) {
        if (ld) {
            const names = ld.fio.split(' ');
            this.lastName = names[0];
            this.firstName = names[1];
            this.middleName = names[2];
            this.relativeId = ld.relativeId;
            this.docNumber = ld.number;
            const date = ld.birthDate.split('.');
            this.birthDate = {
                year: parseInt(date[2], 10), month: parseInt(date[1], 10),
                day: parseInt(date[0], 10)
            };
        }
    }
}
