import {Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {SearchService} from '../../../services/search.service';
import {Router} from '@angular/router';
import {untilComponentDestroyed} from '@w11k/ngx-componentdestroyed';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {PeopleSearchComponent} from '../../../modals/people-search/people-search.component';
import {LdFilter} from '../../../models/ld-filter';

@Component({
    selector: 'app-main-header',
    templateUrl: './main-header.component.html',
    encapsulation: ViewEncapsulation.None
})
export class MainHeaderComponent implements OnInit, OnDestroy {
    private searchKey = 'MainHeaderComponent';
    request = new LdFilter();
    @ViewChild(PeopleSearchComponent, {static: true}) searchModal: PeopleSearchComponent;

    constructor(private ss: SearchService, private router: Router, private sms: NgxSmartModalService) {
    }

    ngOnInit() {
    }

    search() {
        this.searchModal.request = this.request;
        this.searchModal.open(true);
        // this.sms.getModal('people-search-modal').open();
        return;
        // TODO не правильно работает, получает старые значения, т.к. используется BehaviorSubject. нужно что-то придумать.
        this.ss.getPeople(this.searchKey).pipe(untilComponentDestroyed(this))
            .subscribe(res => {
                console.log(res);
                if (res) {
                    if (res.count === 0) {
                        this.router.navigate(['lichnoe-delo'], {
                            // queryParams: this.searchFg.value
                        });
                    } else if (res.count === 1) {
                        console.log(res.data);
                        // this.router.navigate(['lichnoe-delo'], {queryParams: {...res.item[0]}});
                    } else {
                        console.log(res.data);
                        // this.router.navigate([Path.KidRegistry]);
                    }
                }
            });
        // this.ss.findPeople(group, this.searchKey);
    }

    ngOnDestroy(): void {
    }
}

