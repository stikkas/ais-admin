package ru.insoft.aiscustody.services

import ru.insoft.aiscustody.models.Classifier
import ru.insoft.aiscustody.models.Dict

interface DictService {
    fun classifier(cls: Classifier): List<Dict>
}

