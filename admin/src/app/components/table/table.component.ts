import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    QueryList,
    ViewChildren,
    ViewEncapsulation
} from '@angular/core';
import {SortEvent} from '../../models/sort-event';
import {SortableDirective} from '../../directives/sortable.directive';

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    encapsulation: ViewEncapsulation.None
})
export class TableComponent implements OnInit {
    @Input() title: string;
    @Input() headers: string[];
    @Input() wrapClass: string;
    @Input() width: string;
    @Input() sorts: any[];
    @Output() sort = new EventEmitter<SortEvent>();

    @ViewChildren(SortableDirective) sortHeaders: QueryList<SortableDirective>;

    constructor() {
    }

    ngOnInit() {
    }

    setSort(event: SortEvent) {
        this.sort.emit(event);
        if (this.sortHeaders) {
            this.sortHeaders.forEach(header => {
                if (header.sortable !== event.column) {
                    header.clear();
                }
            });
        }
    }

    clearSorts() {
        this.sortHeaders.forEach(header => header.clear());
    }
}
