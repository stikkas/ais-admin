import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {ModalComponent} from '../modal/modal.component';
import {SearchService} from '../../services/search.service';
import {SortEvent} from '../../models/sort-event';
import {SearchResultComponent} from '../../components/search-result/search-result.component';
import {LdTableRow} from '../../models/ld-table-row';
import {PsBarComponent} from '../../components/ps-bar/ps-bar.component';
import {LdFilter} from '../../models/ld-filter';

@Component({
    selector: 'app-family-member-add',
    templateUrl: './family-member-add.component.html',
    styleUrls: ['./family-member-add.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class FamilyMemberAddComponent implements OnInit {
    private searchKey = 'FamilyMemberAddComponent';
    inputLd: LdTableRow;
    @Output() done = new EventEmitter<LdTableRow>();
    @ViewChild('mc', {static: true}) mc: ModalComponent;
    @ViewChild(SearchResultComponent, {static: true}) resultTable;
    @ViewChild(PsBarComponent, {static: true}) psBar;
    @ViewChild('remark', {static: true}) remark: ElementRef;

    page$ = this.ss.getPeople(this.searchKey);
    request = new LdFilter();
    current: LdTableRow;

    constructor(private ss: SearchService) {
    }

    ngOnInit() {
    }

    search(event?: SortEvent) {
        if (!event) {
            this.resultTable.clearSorts();
        }
        this.ss.findPeople(this.request, this.searchKey, event);
    }

    accept(p: LdTableRow) {
        if (p) {
            this.done.emit({
                ...p, remark: this.remark.nativeElement.value, sex: this.psBar.relation(),
                relativeId: this.request.relativeId
            });
            this.clear();
        }
    }

    open(run = false) {
        this.mc.modal.open();
        if (run) {
            this.search();
        }
    }

    private clear() {
        this.resultTable.clearSorts();
        this.remark.nativeElement.value = '';
        this.ss.clear(this.searchKey);
        this.mc.close();
        this.request = new LdFilter();
    }
}

