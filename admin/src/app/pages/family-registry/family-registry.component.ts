import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-family-registry',
  templateUrl: './family-registry.component.html',
  styles: [],
  encapsulation: ViewEncapsulation.None
})
export class FamilyRegistryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
