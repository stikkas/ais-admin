package ru.insoft.aiscustody.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.insoft.aiscustody.models.Classifier
import ru.insoft.aiscustody.services.DictService

@RestController
@RequestMapping("/api/private/dicts")
class DictController(private val dictService: DictService) {

    @GetMapping("/family-members")
    fun familyMembers() = dictService.classifier(Classifier.FAMILY_MEMBERS)

}

