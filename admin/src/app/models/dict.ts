export interface Dict {
    value: string;
    label: string;
}
