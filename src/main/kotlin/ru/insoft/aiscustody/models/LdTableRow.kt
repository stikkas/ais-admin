package ru.insoft.aiscustody.models

class LdTableRow {
    var systemmanId: String? = null
    var fio: String? = null
    // Для детей - пол, для всех остальных - родственное отношение, например, Мать
    var sex: String? = null
    var birthDate: String? = null
    var deathDate: String? = null
    var documentType: String? = null
    var seria: String? = null
    var number: String? = null
    var relativeId: String? = null
}

