package ru.insoft.aiscustody.repo

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import ru.insoft.aiscustody.models.Classifier
import ru.insoft.aiscustody.models.Dict

@Mapper
interface ClassifierMapper {
    fun classifier(@Param("cls") cls: Classifier): List<Dict>
}
