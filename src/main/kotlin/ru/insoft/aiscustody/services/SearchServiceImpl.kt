package ru.insoft.aiscustody.services

import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.insoft.aiscustody.filters.LdFilter
import ru.insoft.aiscustody.models.Members
import ru.insoft.aiscustody.models.Page
import ru.insoft.aiscustody.offset
import ru.insoft.aiscustody.repo.LdMapper
import ru.insoft.aiscustody.repo.MembersMapper

@Service
@Transactional(readOnly = true)
class SearchServiceImpl(private val ldMapper: LdMapper, private val membersMapper: MembersMapper) : SearchService {

    override fun findAllLd(filter: LdFilter, page: Long, size: Int) = runBlocking {
        val data = async { ldMapper.findAll(filter, page.offset(size), size) }
        val count = async { ldMapper.count(filter) }
        Page(data.await(), size, count.await(), page)
    }

    override fun findAllMembers(mainId: String, childs: List<String>) = runBlocking {
        val parents = async { membersMapper.findParents(mainId) }
        val relatives = async { membersMapper.findRelatives(mainId) }
        val siblings = async { membersMapper.findSiblings(listOf(mainId), listOf(mainId)) }
        var all = childs + mainId
        var kids = membersMapper.findSiblings(all, all)
        var allkids = kids
        while (kids.isNotEmpty()) {
            val found = kids.map { it.systemmanId!! }
            all = found + all
            kids = membersMapper.findSiblings(found, all)
            allkids += kids
        }
        Members(allkids, parents.await(), relatives.await(), siblings.await().map { it.systemmanId!! })
    }
}
