import {Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute} from '@angular/router';
import {SearchService} from '../../services/search.service';
import {LdTableRow} from '../../models/ld-table-row';
import {untilComponentDestroyed} from '@w11k/ngx-componentdestroyed';
import {MembersService} from '../../services/members.service';
import {PeopleSearchComponent} from '../../modals/people-search/people-search.component';
import {LdFilter} from '../../models/ld-filter';
import {FamilyMemberAddComponent} from '../../modals/family-member-add/family-member-add.component';

export const ageBorder = 1000 * 60 * 60 * 24 * (365 * 18 + 5);

@Component({
    selector: 'app-bio-family',
    templateUrl: './bio-family.component.html',
    encapsulation: ViewEncapsulation.None
})
export class BioFamilyComponent implements OnInit, OnDestroy {
    private selectedChildren = [] as LdTableRow[];

    @ViewChild(PeopleSearchComponent, {static: true}) searchModal: PeopleSearchComponent;
    @ViewChild(FamilyMemberAddComponent, {static: true}) addMemsModal: FamilyMemberAddComponent;

    ctxMenuX = '0';
    ctxMenuY = '0';
    kidsMenu: boolean;
    relativesMenu: boolean;
    currents = {parent: null as LdTableRow, relative: null as LdTableRow, child: null as LdTableRow};
    tableHeaders = [
        'Фамилия Имя Отчество',
        '',
        'Дата<br>рождения',
        'Документ',
        'Серия',
        'Номер',
        'Дата<br>смерти',
        'Примечание'
    ];
    siblings: string[];
    children: LdTableRow[];
    relatives: LdTableRow[];
    parents: LdTableRow[];
    relatives$ = this.ms.getRelatives();
    minRows = 2;
    choosenRelative: LdTableRow;

    constructor(private sms: NgxSmartModalService, title: Title, private route: ActivatedRoute,
                private ss: SearchService, private ms: MembersService) {
        title.setTitle('Опека - Биологическая семья');
        this.ms.getChildren().pipe(untilComponentDestroyed(this)).subscribe(kids => {
            if (kids && kids.length) {
                this.children.push(...kids.filter(it => !this.children.some(i => i.systemmanId === it.systemmanId)));
            }
        });
        this.ms.getCurrentMembers().pipe(untilComponentDestroyed(this)).subscribe(mems => {
            if (mems) {
                this.siblings = mems.siblings;
                this.children.push(...mems.kids.filter(it => !this.children.some(i => i.systemmanId === it.systemmanId)));
                this.relatives = mems.relatives;
                this.parents = mems.parents;
            } else {
                this.relatives = null;
                this.parents = null;
                this.siblings = null;
            }
        });
    }

    ngOnInit() {
    }

    ngOnDestroy(): void {
    }

    showCopyFamily(c: LdTableRow) {
        const ids = [];
        this.children.forEach(it => {
            if (it.systemmanId !== c.systemmanId) {
                ids.push(it.systemmanId);
            }
        });
        if (ids.length) {
            this.ms.findRelatives(ids);
            this.sms.getModal('family-copy-modal').open();
        }
    }

    showPeopleSearch() {
        this.disableCtxMenu();
        this.searchModal.request = new LdFilter();
        this.searchModal.request.child = true;
        this.searchModal.open();
    }

    showAddMembers(create = true) {
        this.disableCtxMenu();
        if (create) {
            this.addMemsModal.open();
        } else {
            this.addMemsModal.request = new LdFilter(this.choosenRelative);
            this.addMemsModal.inputLd = this.choosenRelative;
            this.addMemsModal.open(true);
        }
    }

    showHideAllMembers(checked: boolean) {
        console.log((checked ? 'Показать' : 'Скрыть') + ' всех');
    }

    showHideActual(checked: boolean) {
        console.log((checked ? 'Показать' : 'Скрыть') + ' актуальных');
    }

    showKidsMenu(event: MouseEvent, kid: LdTableRow) {
        this.kidsMenu = true;
        this.ctxMenuX = `${event.clientX}px`;
        this.ctxMenuY = `${event.clientY}px`;
    }

    showRelativesMenu(event: MouseEvent, r: LdTableRow) {
        this.choosenRelative = r;
        this.relativesMenu = true;
        this.ctxMenuX = `${event.clientX}px`;
        this.ctxMenuY = `${event.clientY}px`;
    }

    disableCtxMenu() {
        this.kidsMenu = false;
        this.relativesMenu = false;
    }

    selected(c: LdTableRow): boolean {
        return this.selectedChildren.indexOf(c) > -1;
    }

    select(c: LdTableRow) {
        const idx = this.selectedChildren.indexOf(c);
        if (idx > -1) {
            this.selectedChildren.splice(idx, 1);
        } else {
            this.selectedChildren.push(c);
        }
    }

    childChanged(c: LdTableRow) {
        if (this.currents.child !== c) {
            this.currents.child = c;
            this.ms.loadMembers(c.systemmanId, this.children.filter(it => it !== c).map(it => it.systemmanId));
        }
    }

    addChild(child: LdTableRow) {
        this.children = this.children || [];
        const exists = this.children.find(it => it.systemmanId === child.systemmanId);
        if (!exists) {
            this.children.push(child);
        }
        this.childChanged(exists || child);
    }

    isSibling(c: LdTableRow) {
        return c !== this.currents.child && this.siblings && this.siblings.some(it => it === c.systemmanId);
    }

    addRelatives(relatives: LdTableRow[]) {
        relatives.forEach(it => this.addRelative(it, false));
    }

    addRelative(relative: LdTableRow, searchChildren = true) {
        const rel = relative.sex.toLowerCase();
        this.parents = this.parents || [];
        this.relatives = this.relatives || [];
        if (['мать', 'отец'].indexOf(rel) > -1) {
            if (!this.parents.some(it => it.systemmanId === relative.systemmanId)) {
                this.parents.push(relative);
                if (searchChildren) {
                    this.ms.findChildren(relative.systemmanId);
                }
                const idx = this.relatives.findIndex(it => it.systemmanId === relative.systemmanId);
                if (idx > -1) {
                    this.relatives.splice(idx, 1);
                }
            }
        } else {
            if (!this.relatives.some(it => it.systemmanId === relative.systemmanId)) {
                this.relatives.push(relative);
                const idx = this.parents.findIndex(it => it.systemmanId === relative.systemmanId);
                if (idx > -1) {
                    this.parents.splice(idx, 1);
                }
            }
        }
        this.ms.addMember(this.currents.child.systemmanId, relative.systemmanId, relative.relativeId);
    }


    private toDate(d: string | Date) {
        let res = null;
        if (d) {
            if (typeof d === 'string') {
                const x = d.split('.');
                res = new Date([x[2], x[1], x[0]].join('/'));
            } else {
                res = d;
            }
        }
        return res;
    }


}

