package ru.insoft.aiscustody.formatters

import org.springframework.format.Formatter
import ru.insoft.aiscustody.models.Classifier
import java.util.*


open class ClassifierFormatter : Formatter<Classifier> {

    override fun print(obj: Classifier, locale: Locale): String = obj.toString()

    override fun parse(json: String, locale: Locale) = Classifier.toClassifier(json)
            ?: throw RuntimeException("Неправильный идентификатор")
}

