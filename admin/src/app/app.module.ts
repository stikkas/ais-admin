import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import localeRuExtra from '@angular/common/locales/ru';
import {registerLocaleData} from '@angular/common';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MainComponent} from './pages/main/main.component';
import {MainHeaderComponent} from './pages/main/main-header/main-header.component';
import {BioFamilyComponent} from './pages/bio-family/bio-family.component';
import {BfHeaderComponent} from './pages/bio-family/bf-header/bf-header.component';
import {FamilyCopyComponent} from './modals/family-copy/family-copy.component';
import {NgxSmartModalModule} from 'ngx-smart-modal';
import {PeopleSearchComponent} from './modals/people-search/people-search.component';
import {ModalComponent} from './modals/modal/modal.component';
import {FamilyMemberAddComponent} from './modals/family-member-add/family-member-add.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TableComponent} from './components/table/table.component';
import {NotifierModule} from 'angular-notifier';
import {HttpClientModule} from '@angular/common/http';
import {LoadComponent} from './components/load/load.component';
import {SortableDirective} from './directives/sortable.directive';
import {CandidateRegistryComponent} from './pages/candidate-registry/candidate-registry.component';
import {ChildRegisteredComponent} from './pages/child-registered/child-registered.component';
import {ChildRegistryComponent} from './pages/child-registry/child-registry.component';
import {FamilyRegistryComponent} from './pages/family-registry/family-registry.component';
import {PremsRegistryComponent} from './pages/prems-registry/prems-registry.component';
import {ReportsComponent} from './pages/reports/reports.component';
import {MenuBarComponent} from './components/menu-bar/menu-bar.component';
import {PsBarComponent} from './components/ps-bar/ps-bar.component';
import {SearchResultComponent} from './components/search-result/search-result.component';
import {LichnoeDeloComponent} from './pages/lichnoe-delo/lichnoe-delo.component';
import {NgbDateParserFormatter, NgbDatepickerConfig, NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {MaskDateDirective} from './directives/mask-date.directive';
import {NgbDateCustomParserFormatter} from './utils/ngb-date-custom-parser-formatter';
import {ConfirmInfoComponent} from './modals/confirm-info/confirm-info.component';
import {ConfirmRemoveComponent} from './modals/confirm-remove/confirm-remove.component';
import {ConfirmSaveComponent} from './modals/confirm-save/confirm-save.component';

registerLocaleData(localeRuExtra);

@NgModule({
    declarations: [
        AppComponent,
        MainComponent,
        MainHeaderComponent,
        BioFamilyComponent,
        BfHeaderComponent,
        FamilyCopyComponent,
        PeopleSearchComponent,
        ModalComponent,
        FamilyMemberAddComponent,
        TableComponent,
        LoadComponent,
        SortableDirective,
        CandidateRegistryComponent,
        ChildRegisteredComponent,
        ChildRegistryComponent,
        FamilyRegistryComponent,
        PremsRegistryComponent,
        ReportsComponent,
        MenuBarComponent,
        PsBarComponent,
        SearchResultComponent,
        LichnoeDeloComponent,
        MaskDateDirective,
        ConfirmInfoComponent,
        ConfirmRemoveComponent,
        ConfirmSaveComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        NgbDatepickerModule,
        NotifierModule.withConfig({
            behaviour: {
                autoHide: 3000
            },
            position: {
                horizontal: {
                    position: 'right'
                },
                vertical: {
                    position: 'top'
                }
            }
        }),
        NgxSmartModalModule.forRoot()
    ],
    providers: [{provide: LOCALE_ID, useValue: 'ru'}, {
        provide: NgbDateParserFormatter,
        useClass: NgbDateCustomParserFormatter
    }],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(config: NgbDatepickerConfig) {
        config.minDate = {year: 1900, month: 1, day: 1};
        config.maxDate = {year: 2099, month: 12, day: 31};
    }
}
