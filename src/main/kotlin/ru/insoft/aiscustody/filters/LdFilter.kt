package ru.insoft.aiscustody.filters

import org.springframework.format.annotation.DateTimeFormat
import ru.insoft.aiscustody.models.Classifier
import java.net.URLDecoder
import java.util.*

/**
 * Критерии поиска по личным делам
 */
class LdFilter {

    /**
     * Дата рождения 'с'
     */
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    var birthDate: Date? = null
        get() = if (child) { // для детей корректируем минимальную дату рождения
            minBirthDate = minBirthDate ?: Calendar.getInstance().run {
                add(Calendar.YEAR, -18)
                add(Calendar.DATE, 1)
                set(Calendar.HOUR_OF_DAY, 0)
                set(Calendar.MINUTE, 0)
                set(Calendar.SECOND, 0)
                set(Calendar.MILLISECOND, 0)
                field?.let { d ->
                    maxOf(time, d)
                } ?: time
            }
            minBirthDate
        } else {
            field
        }


    /**
     * Фамилия
     */
    var lastName: String? = null
        set(value) {
            field = value?.let { URLDecoder.decode(it, "UTF-8")?.trim()?.toLowerCase() }
        }
    /**
     * Имя
     */
    var firstName: String? = null
        set(value) {
            field = value?.let { URLDecoder.decode(it, "UTF-8")?.trim()?.toLowerCase() }
        }

    /**
     * Отчество
     */
    var middleName: String? = null
        set(value) {
            field = value?.let { URLDecoder.decode(it, "UTF-8")?.trim()?.toLowerCase() }
        }

    /**
     * Номер удостоверяющего документа
     */
    var docNumber: String? = null
        set(value) {
            field = value?.let { URLDecoder.decode(it, "UTF-8")?.trim()?.toLowerCase() }
        }

    /**
     * Поле для сортировки
     */
    var sortField: String? = null

    /**
     * Направление сортировки
     */
    var desc: Boolean = false

    /**
     * Родственные отношения
     */
    var relativeId: Classifier? = null
        set(value) = when (value) {
            Classifier.FATHER, Classifier.ANLCE, Classifier.GRANDPA, Classifier.BROTHER -> sex = 0
            Classifier.MOTHER, Classifier.AUNT, Classifier.GRANDMA, Classifier.SISTER -> sex = 1
            else -> {
            }
        }

    /**
     * Пол проставляем в зависимости от родственных отношений
     */
    var sex: Int? = null
        private set

    /**
     * Ищем детей
     */
    var child: Boolean = false

    private var minBirthDate: Date? = null
}

