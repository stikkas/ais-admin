import {ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {actions} from 'src/app/consts/actions';

@Component({
    selector: 'app-menu-bar',
    templateUrl: './menu-bar.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuBarComponent implements OnInit {
    actions = actions;

    constructor() {
    }

    ngOnInit() {
    }
}

