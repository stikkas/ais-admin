import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {actions} from '../../consts/actions';

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit {
    actions = actions;

    constructor(title: Title) {
        title.setTitle('Опека - Главная');
    }

    ngOnInit() {
    }

}
