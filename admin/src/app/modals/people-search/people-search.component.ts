import {Component, EventEmitter, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {SearchService} from '../../services/search.service';
import {SortEvent} from '../../models/sort-event';
import {Person} from '../../models/person';
import {ModalComponent} from '../modal/modal.component';
import {SearchResultComponent} from '../../components/search-result/search-result.component';
import {LdTableRow} from '../../models/ld-table-row';
import {LdFilter} from '../../models/ld-filter';

@Component({
    selector: 'app-people-search',
    templateUrl: './people-search.component.html',
    styleUrls: ['./people-search.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class PeopleSearchComponent {
    private searchKey = 'PeopleSearchComponent';
    @Output() done = new EventEmitter<LdTableRow>();
    request = new LdFilter();

    @ViewChild('mc', {static: true}) mc: ModalComponent;
    @ViewChild(SearchResultComponent, {static: true}) resultTable;

    page$ = this.ss.getPeople(this.searchKey);
    // searchFg: FormGroup;
    current: Person;

    constructor(private ss: SearchService) {
        // this.createFormGroup();
    }

    search(event?: SortEvent) {
        if (!event) {
            this.resultTable.clearSorts();
        }
        this.ss.findPeople(this.request, this.searchKey, event);
    }

    accept(p: LdTableRow) {
        this.done.emit(p);
        this.clear();
    }

    open(run = false) {
        this.mc.modal.open();
        if (run) {
            this.search();
        }
    }

    private clear() {
        this.resultTable.clearSorts();
        this.ss.clear(this.searchKey);
        this.mc.close();
        // this.createFormGroup();
    }

    // private createFormGroup() {
    //     this.searchFg = this.ss.formGroup;
    //     this.searchFg.controls.child.setValue(true, {onlySelf: true, emitEvent: false});
    // }
}

