import {Observable, of} from 'rxjs';
import {JsonResponse} from '../models/json-response';
import {catchError, map} from 'rxjs/operators';
import {Errorable} from './errorable';

export abstract class Responsable extends Errorable {
    protected handle<T>(obs: Observable<JsonResponse<T>>): Observable<T> {
        return obs.pipe(map((res: JsonResponse<T>) => res.status ? res.data : this.errorHandler({message: res.data})),
            catchError(err => of(this.errorHandler(err))));
    }
}
