package ru.insoft.aiscustody.models

class Ld {
    var systemmanId: String? = null
    var fio: String? = null
    var birthDate: String? = null
    var systemAddressId: String? = null
    var address: String? = null
    var registrationDate: String? = null
    var documentType: String? = null
    var seria: String? = null
    var number: String? = null
    var registrationId: String? = null
    var flat: String? = null
}
