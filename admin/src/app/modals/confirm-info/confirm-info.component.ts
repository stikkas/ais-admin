import {ChangeDetectionStrategy, Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'app-confirm-info',
    templateUrl: './confirm-info.component.html',
    styleUrls: ['./confirm-info.component.css'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmInfoComponent implements OnInit {
    @Input() text: string;

    constructor() {
    }

    ngOnInit() {
    }
}

