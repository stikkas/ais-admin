import {Component, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {ModalComponent} from '../modal/modal.component';
import {LdTableRow} from '../../models/ld-table-row';

@Component({
    selector: 'app-family-copy',
    templateUrl: './family-copy.component.html',
    styleUrls: ['./family-copy.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class FamilyCopyComponent implements OnInit {
    headers = ['ФИО', 'Дата<br>рождения', 'Родственное<br>отношение', '&nbsp;'];
    maxRows = 5;

    @ViewChild('mc', {static: true}) mc: ModalComponent;
    @Input() data: LdTableRow[];
    @Output() accept = new EventEmitter<LdTableRow[]>();

    private choosen: LdTableRow[] = [];

    constructor() {
    }

    ngOnInit() {
    }

    selected(it: LdTableRow) {
        return this.choosen.indexOf(it) > -1;
    }

    select(it: LdTableRow) {
        const idx = this.choosen.indexOf(it);
        if (idx > -1) {
            this.choosen.splice(idx, 1);
        } else {
            this.choosen.push(it);
        }
    }

    save() {
        this.accept.emit(this.choosen);
        this.mc.close();
        this.choosen = [];
    }
}

