package ru.insoft.aiscustody

import org.springframework.context.annotation.Configuration
import org.springframework.format.FormatterRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import ru.insoft.aiscustody.formatters.ClassifierFormatter

@Configuration
class MvcConfig : WebMvcConfigurer {

    override fun addFormatters(registry: FormatterRegistry) {
        registry.addFormatter(ClassifierFormatter())
    }
}

