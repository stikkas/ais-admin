import {ChangeDetectionStrategy, Component, ViewEncapsulation} from '@angular/core';
import {LoadingService} from '../../services/loading.service';

@Component({
    selector: 'app-load',
    template: `
        <div *ngIf="loading$|async" class="b-loader">
            <div class="b-loader__icon"></div>
        </div>
    `,
    styles: [`
        .b-loader {
            z-index: 1042 !important;
        }
    `],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoadComponent {
    loading$ = this.ls.loading;

    constructor(private ls: LoadingService) {
    }
}
