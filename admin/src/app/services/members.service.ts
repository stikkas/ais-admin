import {Injectable} from '@angular/core';
import {NotifierService} from 'angular-notifier';
import {HttpClient} from '@angular/common/http';
import {LoadingService} from './loading.service';
import {Subject} from 'rxjs';
import {Members} from '../models/members';
import {JsonResponse} from '../models/json-response';
import {Responsable} from './responsable';
import {LdTableRow} from '../models/ld-table-row';

@Injectable({
    providedIn: 'root'
})
export class MembersService extends Responsable {
    private baseUrl = '/api/members/';
    private curMems$ = new Subject<Members>();
    private children$ = new Subject<LdTableRow[]>();
    private relatives$ = new Subject<LdTableRow[]>();

    constructor(private http: HttpClient, ns: NotifierService,
                private ls: LoadingService) {
        super(ns);
    }

    loadMembers(mainId: string, childs: string[]) {
        this.ls.load(true);
        this.handle(
            this.http.get<JsonResponse<Members>>(`${this.baseUrl}${mainId}`, {params: {childs}})
        ).subscribe(res => {
            this.ls.load(false);
            this.curMems$.next(res);
        });
    }

    /**
     * Возвращает список родственников ребенка
     */
    getCurrentMembers() {
        return this.curMems$.asObservable();
    }

    /**
     * Возвращает детей какого-либо из родителей
     */
    getChildren() {
        return this.children$.asObservable();
    }

    /**
     * Добавляет нового члена семьи к ребенку
     * @param mainId - id ребенка
     * @param systemmanId - id нового члена
     * @param relativeId - id родственного отношения между ребенком и новым членом
     */
    addMember(mainId: string, systemmanId: string, relation: string) {
        this.handle(
            this.http.post<JsonResponse<any>>(`${this.baseUrl}${mainId}`,
                null, {params: {systemmanId, relation}})
        ).subscribe();
    }

    /**
     * Возвращает всех детей для systemmanId, и детей родителей найденных детей
     * @param systemmanId - идентификатор родителя
     */
    findChildren(systemmanId: string) {
        this.ls.load(true);
        this.handle(
            this.http.get<JsonResponse<LdTableRow[]>>(`${this.baseUrl}children/${systemmanId}`)
        ).subscribe(res => {
            this.ls.load(false);
            this.children$.next(res);
        });
    }

    findRelatives(ids: string[]) {
        this.ls.load(true);
        this.handle(
            this.http.get<JsonResponse<LdTableRow[]>>(`${this.baseUrl}relatives`, {params: {ids}})
        ).subscribe(res => {
            this.ls.load(false);
            this.relatives$.next(res);
        });
    }

    getRelatives() {
        return this.relatives$.asObservable();
    }
}

